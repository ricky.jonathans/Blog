---
title: Creatiever worden [WEEK 4]
subtitle: Het visualiseren van mijn onderzoeken
date: 2018-03-10
tage: ["onderzoek"]
---

# Maandag
10:00 Ik ben begonnen aan het visualeren van de interview. Een duidelijk overzicht was wel handig voor mijn team. Alle interview hebben wij bij elkaar verzameld en vervolgens een conclusie getrokken. Wat zijn de probleemstelling en
wat zijn dan de interesse en behoeftes van de gebruiker.  Daarnaast heb ik ook een lifestyle diary afgemaakt. In vakantie heb ik mijn levenstijl bijgehouden met foto's van 48 uur.
Ook voor de vakantie heb ik mijn levenstijl vastgezet.

13:15 Samen met mijn team hebben we gezamelijk de SWOT analyse besproken. Mike heeft het vervolgens verder verwerkt.

# Dinsdag
Hoorcollege

# Woensdag
10:00 Mijn laatste onderzoeksmethode "de 5 why" is eindelijk af. Afgelopen 2 weken heb ik aantal sportscholen en een bedrijf gevraagd voor een interview. Een veganist, nutritionist, basic fitnes coach en
persoon dan al 4 jaar lang een gezonde levenstijl leidt. Hun hebben de ervaring en kennis die ik nodig heb voor mijn onderzoek. 

Ik heb de vragen en antwoorden van mijn testpersonen verwerkt en vervolgens visueel gemaakt. De resultaten van de 5 why zijn zeker informatief voor ons onderzoek. 

# Donderdag
Vandaag heb ik weer keuzevak gevolgd making movie. Niet zo interessant om eerlijk gezegd. De uitleg was zeer nutteloos. 10 minuten lang waarom een muis handig is voor het editen 
vind ik overdreven en niet informatief. Ik ben blij dat ik elke avond zelf Adobe Premiere aanleerd.
# Vrijdag

# Zaterdag

# Zondag