---
title: Kill your Darling is moeilijk [WEEK 4]
subtitle: Demo testen concept
date: 2017-12-09
tage: ["paard"]
---


# Maandag
Vandaag hebben we gefocust op het concept en de low prototype. Doormiddel van de onderzoeksvragen interview (Joshoa en Rick) en enquete (Serhat en Fabian) gingen wij brainstormen.
We bedachten 3 concepten. Vervolgens hebben we de 3 concepten in 1 geheel gemaakt. Ook hebben we besproken over de low fid prototype. Ik vroeg aan mijn teamgenoten welke zaken er
terug moet komen te zien in te low prototype, zodat ik morgen avond rekening kan houden met het maken van de low fid prototype.

# Dinsdag
Vandaag begon mijn 1e hoorcollege over “De mens”. Een presentatie van over “Sociale beïnvloeding”. Het was voor mij langdradig en oninteressant, maar het kan zijn
dat ik slecht had geslapen. Het was een presentatie over	hoe	we	denken	over	de	sociale	context	en	hoe	dit	ons	gedrag	beïnvloed.	
Ik leerde voornamelijk over het emotie en motivatie. Het verschil tussen automatisch en gecontroleerd denken. Ik heb helaas weinig aantekeningen gemaakt.

Avonduren heb ik de low fid prototype uitgewerkt en feedback opgevraagd aan mijn projectgroepje.
![lowfidprototype](../img/lowfidprototype.jpg)
(afb.1: lowfidprototype over ons concept)

# Woensdag
Vandaag tussen 9.30 - 12.00 was de pitchen voor de opdrachtgever voor alle studio's. Ons concept is door de grond geboord. Of te wel tijd voor "kill your darling."
We hebben wel feedback gekregen dat we zeker kunnen gebruiken in ons volgende vernieuwde concept.

# Donderdag
 
# Vrijdag
Vandaag heb ik weer engels bijles gevolgd over grammatica. Ook heb ik om 13:00 uur samen met Joshoa van den Hoek de merkanalyse gemerkt. Gelukkig hebben we geen onvoldoende, maar 
we moesten wel aantal punten verbeteren. Of in andere woorden, we moeten volgende week WEER TERUGKOMEN :(.

# Zaterdag
# Zondag