---
title: Drukke week met deadlines [week 5]
subtitle: Geen tijd om deze week blog online te zetten
date: 2017-10-06
tage: ["blog"]
---

Ik had helaas geen tijd om de blog online te zetten van deze week. Hieronder ziet u mijn 
dagelijkse bezigheid wat ik heb gedaan en geleerd.

# Maandag
Vandaag had ik een workshop gevolgd over het toevoegen van een thema in een blog. 
Dat verliep zo slecht dat niks meer deed in mijn blog, waardoor ik besloot om weer alles 
op een word-bestand te zetten. Gelukkig had ik een globale backup van mijn post, 
maar dat stond allemaal in een word-bestand. Het verbinden bia Smartgit werkte ook niet mee.

Hiervoor ga ik mijn medestudenten vragen voor hulp.

Ik heb verder feedback gevraagd aan 3 mensen binnen mijn projectgroep via een 
peerfeedback kaart. Dankzij de feedback weet ik wat ik de komende tijd moet verbeteren.


# Dinsdag
Vandaag heb ik aan mijn CMD-beroepsprofiel gewerkt en ingeleverd. In de middag uren heb ik 17 prototyope schermen gemaakt via photoshop. Dat heb ik vervolgens gedeeld aan mijn
projectgroep en andere gebruikers voor feedback en verbeteringen.
(foto)

Later in de avond uren vroeg ik aan Indy over hij alvast een begin kon maken aan de powerpoint, zodat ik later verder kon werken.
Helaas was ik zeer ontevreden toen ik zijn powerpoint zag, waardoor ik het opnieuw moest gaan maken.

Aangezien Indy ons concept gaat presenteren moest hij op de hoogte zijn wat over de powerpoint. Ook had ik iemand nodig voor feedback van mijn powerpoint.
Zo hij bood zichzelf aan om samen het af te maken tot half 2 snacht, zodat we samen een mooi presentatie leveren aan het publiek.

Wel heb ik geleerd om beter aan ons deadlines te houden. 
(foto)

# Woensdag
In de ochtend hebben Lars, Indy en Kim gepresenteerd over ons concept. We kregen verschillende, tegenstrijdige , maar ook goede feedback binnen. Dat gaan we voor de volgende 
iteratie gebruiken.

Later in de middag uren hadden we geen speelbare app. Gelukkig kregen we een goude tip om Marvel te gebruiken.
De schermen hadden we toegevoegd op mijn telefoon en vervolgens hebben we het gefilmd. De marvel app is een zeer handig hulpmiddel voor een prototype.
Dat gaan we in de toekomst waarschijnlijk vaker gebruiken.


# Donderdag
-

# Vrijdag
-
