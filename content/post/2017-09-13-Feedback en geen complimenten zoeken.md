---
title: Vragen naar feedback. Niet naar complimenten
subtitle: Feedback en geen complimenten
date: 2017-09-13
tage: ["paperprototype", "feedback"]
---


Vandaag gaan wij ons paperprototype presenteren  en vragen naar feedback. Wat ik hieruit heb geleerd is dat je niet opzoek moet gaan naar complimentjes of goede punten.
Dat kregen we tevaak tehoren tijdens onze kleine presentatie van de paperprototype. Het geef je wel een goed gevoel dat de tevredenheid van de meeste mensen bij ons
concept hoog is. Maar hierdoor verbeter je niet de concept. Uiteindelijk waren er paar mensen die goede feedback gaven. Met de feedback die wij kregen, zochten wij naar
de juiste oplossing.

Naast onze presentatie ging ik ook  bij andere projectgroepen kijken. Ik zocht naar inspiratie, wat ik ook kon gebruiken voor onze concepten. Ik kwam tot de conclusie dat er
vele manieren zijn om jouw concept te laten zien en te overtuigen.
