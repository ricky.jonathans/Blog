---
title: Niet meer concepten afkraken van elkaar
subtitle: Goede punten eruit halen
date: 2017-09-21
tage: ["concepten", "afkraken"]
---

Het was weer een vroege ochtend photoshoppen of toch niet. Ik kwam anderhalf uur te laat, waardoor ik nog een half uur les had.
En helaas kreeg ik vandaag weer geen uitdagende opdracht. De class en home opdracht was het werken met lagen. En binnen de half uur
had ik alles af.

Ik begreep dat de photoshop stoffen in de komende tijd wel uitdagender wordt. Maar in de tussentijd volg ik thuis regelmatig turtorials over bepaalde technieken in photoshop
die ik kan gebruiken in de toekomst. Hierdoor krijg ik studiepunten voor het volgen van photoshop en doe ik zelfstudie om mijn kennis te verbeteren.

Eenmaal thuis zat ik paar spelanalyse te maken en paar concepten verzinnen. Ik kwam later erachter dat we al die tijd
alleen maar elkaars concepten en spelanalyse af zaten te kraken. In plaats daarvan zocht ik naar de goede punten van 
ieders concept, ideeen en inspiratiebronnen(spelanalyse). Dat ga ik morgen op mijn vrije dag uitwerken en delen met mijn projectgroep. 

