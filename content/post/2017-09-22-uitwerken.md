---
title: Uitwerking van mijn concept
subtitle: Visueel overzichtelijk proces
date: 2017-09-22
tage: ["concepten", "uitwerken"]
---

Vandaag  heb ik mijn spelanalyse afgekregen. Doormiddel van deze informative, visuele inzicht die ik heb, kan ik mijn concept uitwerken en verbeteren. 
Dankzij de spelanalyse en de goede punten van de concepten die wij hadden,  heb ik meer inspiratie en ideeen die ik kan gebruiken.

Vandaag ga ik aan mijn proces van het spel uitwerken met behulp van photoshop (zie afb.1). Ik wil mijn projectgroep een beter beeld geven over mijn idee.

![logo](../img/concept_spelanalyse.jpg)
(afb.1: De resultaat van mijn uitwerking van het concept en een spelanalyse in 1)