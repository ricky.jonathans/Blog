---
title: Feedback vragen
subtitle: Wat kan er beter
date: 2017-09-27
tage: ["concept", "wk4"]
---

Vandaag had ik mijn spelanalyse af en heb ik het ingeleverd in een google drive van Creative. Verder heb ik aan mijn medestudenten uit klas CMD1F gevraagd voor feedback over ons
ruwe concept.
Ik heb vervolgens alle feedback, verbeter punten of zelf ideeen genoteerd en gedeeld met mijn projectgroepje, zodat ze ook op de hoogte zijn.