---
title: Is ons proces goed verlopen of toch niet ?
subtitle: Een ontwerpproces kaart maken
date: 2017-09-25
tage: ["ontwerpproceskaart"]
---

Maandagochtend deelde ik mijn concept aan mijn projectgroep. Ze vonden een zeer goede, uitgewerkte idee, maar er waren zeker punten dat beter konden.
We gingen het concept in grote lijnen uitwerken, maar ook dat verliep niet zo goed. We gingen dus een brainstormsessie houden om te kijken hoe wij
dit gaan oplossen en welke punten we moeten gaan richten.

Daarnaast heb ik met mijn projectgroep een ontwerpproces kaart (zie afb.1) gemaakt. We lieten ons ontwerpprocess kaart nakijken voor feedback en het was helaas niet goed.
We hebben vervolgens uitgebreider gemaakt en verbeterd.


Bij zo proceskaart maakt zichtbaar waar een proces verbeterd kan worden. Ik heb
daarnaast een duidelijk beeld hoe ons product word gemaakt en waar het goed of fout gaat in ons proces.
Ook had ik een beeld wat we beter hebben gedaan integenstellig tot ons vorige iteratie.

![bordspel](../img/bordspel-prototype.jpg)
(afb1. verbeterde ontwerpproces kaart)
