---
title: Concept uittesten [WEEK 6]
subtitle: testfase is inzicht
date: 2018-03-24
tage: ["testen"]
---

# Maandag
10:00 Vandaag heb ik met mijn team besproken over wat de situtatie is op dit moment. Waar lopen we achter en wat willen wij nog gaan uitvoeren. Na de 
bespreking ging iedereen aan de slag en begon ik met mijn testplan. De taken waren het opstellen van een testplan. Verschillende methodes en technieken
opzoeken en kijken of het bruikbaar was voor ons testproces. Ik vond methode als "hardop denkmethode". Een methode waarbij ik een beter inzicht zou krijgen
over de gedachtegang van de testpersoon. Ook een gebruikelijk interview methode ga ik gebruiken voor mijn interview. Door middel van de testplan die ik ga maken
wil ik naast dat we de concept gaan testen, ook de behoeftes en wensen van de testpersonen weten.



# Dinsdag
Hoorcollege

# Woensdag
10:00 Vandaag heb ik met mijn team de compententie en beroepsproducten genoteerd ? Wie en heeft wat gemaakt en kunnen zij het gebruiken voor hun leerdossier ? Ik wilde een beter overzicht
hebben over alle  opdrachten die zijn gemaakt in de afgelopen weken. De deadline komt dichterbij en in week 7 moet ik mijn tentamen en leerdossier af zijn. Verder heb ik samen met
Donovan de ontwerpproces kaart afgemaakt. 

15:30 Nadat de testplan is afgemaakt, heb ik paar studenten getest. Wat vonden zij over ons concept en zijn er verbeter punten ? Na het testen kwamen we erachter dat ons concept
niet voldoende is om studenten te stimuleren tot een gezonde levenstijl.  

18:00 De testresultaten heb ik genoteerd en verwerk.

# Donderdag
8:30 - Vroege ochtend. Vandaag heb ik weer keuzevak Making Movie gevolgd. 15 min uitlegd hoe je een scene moet knippen. Dat was voor mij te langdradig en totaal niet leerzaam.
Gelukkig volg ik elke avond tutorials over Adobe Premie en Videography(zelfstudie). Vandaag heb ik ook de eerste thuisopdracht afgemaakt en vervolgens

# Vrijdag
Gewerkt aan de leerdossier. Ik heb me meer gefoccust op de gedragindicatoren. Tijdens het opstellen van mijn leerdossier heb ik goed gekeken naar de feedback die ik had gekregen
van al mijn STARRT van de kwartaal 2.

# Zaterdag

# Zondag