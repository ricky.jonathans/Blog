---
title: Einde inzicht [WEEK 7]
subtitle: Laatste loodjes tellen het zwaarst
date: 2018-03-30
tage: ["leerdossier"]
---

# Maandag
10:00 De laatstse week is inzicht. Samen met mijn team heb ik hebben we kort besproken wat wij nog moeten doen. Ik, Mike en Donovan hebben in de pauze uren feedback ontvangen 
van Elkse. De feedback ging groot en deels over onze onderzoek. Wat er voortaan beter kan is de mindmap beter uitwerken en op de juiste manier gebruiken. 


# Dinsdag
Hoorcollege

# Woensdag
10:00 Vandaag heb ik mijn feedback vastgelegd van mijn gemaakte werk. Ik heb nuttige feedback ontvangen. De feedback ga ik vanavond verwerken en in mijn leerdossier zetten.


# Donderdag
11:00 Vandaag had ik mijn tentamen gemaakt.
# Vrijdag

# Zaterdag

# Zondag