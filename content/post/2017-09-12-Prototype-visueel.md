---
title: Prototype (App versie)
subtitle: Heb ik nog een 2e Prototype erbij gemaakt ? En zijn mijn aantekening verbetert of niet ? Hmmm...
date: 2017-09-12
tage: ["blog", "team"]
---


Naast het feit dat wij een bordspel hebben gemaakt, vond ik het ook genoodzakelijk om een
layout te maken van de app. Hierbij heb ik meerdere verschermen uitgeprint en de ontwerp geschets(zie afb.1).
Zo kunnen we ook de app versie presenteren voor de klas voor een feedback.

![prototype](../img/app-prototype.jpg)
(afb1: schets van de schermen)

Vandaag volgde ik mijn 2e design theory hoorcollege over verbeelden. Ik heb 
veel kennis opgedaan en het was zeer leergierig. Ook zijn er  weer handigen weetjes die ik
kan gebruiken voor mijn design challenge.

Wat ik wel heb gemerkt tijdens de 1e hoorcollege van vorige week is dat ik mijn aantekeningen meer tekst (zie afb.2) is dan visueel.
Hierdoor ben ik meer informatie aan het opschrijven, dan het opletten van de presentatie.

![aantekeningen](../img/HC1.JPG)
(afb.2: meer tekst aantekeningen dan schetsen)

Oplossing hiervoor is bijvoorbeeld vragen aan medestudenten hoe hij of zij hun 
aantekening maken. Ook ff opzoeken op het internet naar voorbeelden naar visuele aantekeningen.
En dat heb ik ook gedaan. Hieronder je mijn 2e aantekening (zie afb.3) van hoorcollege van verbeelden. Het is wel een verbetering, maar
het kan nog beter natuurlijk.

![logo](../img/HC2.JPG)
(afb.3: Verbetering van de aantekeningen)

