---
title: Nieuw begin of toch niet [WEEK 1]
subtitle: Team creative weer bij elkaar ? (kwartaal 1)
date: 2018-02-14
tage: ["team"]
---


# Maandag
10:00-12:00 Een nieuwe dag, een nieuwe week en een nieuwe begin van het 3e kwartaal. 
Vandaag zijn de groepjes bekend en verrassend genoeg was het de exacte groepje van kwartaal 1, 
maar dan zonder Indy. Ik vond het wel fijn, want ik wist dan hoe de samenwerking zou zijn. Maar aan de andere 
kant vond ik dan jammer, want ik bouw geen nieuw ervaring in het gebied van samenwerken met andere medestudenten.

Na wat te hebben besproken is Kim omgeruild met een ene Mike. Helaas is Donovan en Mike afwezig, 
dus zijn Lars en ik begonnen aan de plan van aanpak.

Ik keek aandachtig naar de opdracht en besproken samen met Lars en Donovan (via whatsapp)
de werkproces, taakverdeling, regels etc.

13:00 – Na de studio heb ik een workshop gevolgd over het inrichten van ontwerpcriteria. 

# Dinsdag
17:00 Vandaag heb ik de plan van aanpak afgemaakt. Daarnaast heb ik een digitale scrumboard ontworpen.
Het is een visuele, overzichtelijke, systematisch en strakke dagplanning per studio dag. Elke ochtend van 9:45 tot 10:00 wordt er 
een bespreking gehouden over de voortgang en andere zaken. 


# Woensdag
10:30 Vanochtend hebben wij onze plan van aanpak gevalideerd. Naar mijn mening waren ze zeer tevreden over taken en activiteiten verdeling. Alles was redelijk
overzichtelijk en netjes aangepakt. Er waren wel paar aandachtpunten zoals het maken van een visuele poster. Doormiddel van een poster staat alles netjes en overzichtelijk
in 1 geheel.

11:00-13:00 Ik heb een onderzoek instructie document gemaakt voor het team. Daar staat de taakverdeling en de vragenlijst dat moet worden gesteld voor de komende onderzoeken zoals 
fieldresearch, expert interview, photojourney enzovoort. 
# Donderdag

# Vrijdag

# Zaterdag
# Zondag