---
title: Een goed begin is het halve werk [WEEK 2]
subtitle: Verschillende onderzoeksmethode uitvoeren
date: 2018-02-24
tage: ["onderzoek"]
---

# Maandag
9.45 Vanochtend hebben wij een lang ochtendbespreking gehad. Wat willen wij gaan onderzoeken en hoe ? Mijn taak in deze onderzoek fase is een goed uitgebreid en duidelijke taakverdeling.
Welke methode gaan wij gebruiken en wie wat gaat het doen. 

De eerste stap is een deskresearch. Het verzamelen van de bestaande informatie. Het is belangrijk, zodat wij weten wat de stand van zaken zijn op dit moment.
De deskresearch wordt gedaan door Mike en Dovovan.De verzamelde data gaat Donovan volgende week vervolgens visualiseren .

De volgende stap is het interviewen van studenten uit verschillende locatie's. Hierbij ia Donovan, Mike en ik bij betrokken. 
Na de deskresearch ga ik de onderzoeksvragen voor de interview aanpassen indien het nodig is (instructie)
gewijzigd. 

Stap 3 is meer verdiepen naar de gebruikers. Dat ga ik doen doormiddel van een photojournal. Een nieuwe methode die ik nog nooit heb uitgevoerd. 

De vierde stap is het maken van een Define your Audience. Deze onderzoeksmethode gaat Mike en Lars uitvoeren.

De 5 why methode onderzoek is de volgende stap. Daar gaan wij experts interviewen. Van ervaren sporter tot nutrionist. Verschillende bedrijven ga ik proberen te benaderen, zodat 
ik de juiste informatie kan verzamelen. Dankzij een 5 why methode kan ik de persoon diepere vragen stellen.

Dit zijn ongeveer de stappen die had ingedeeld voor mijn team. Ik vind dat ik verantwoordelijk ben voor de resultaten. De samenwerking binnen het team moet beter zijn dan
vorige kwartalen.



# Dinsdag
17:00 Vandaag heb ik de foto's binnengekregen van een 1e jaar HBO student die studeert aan de Hogeschool Rotterdam en doet de opleiding Fiscaal Recht en Economie. 
Hij heeft de afgelopen dagen wat foto's gestuurd over zijn dagelijks leven. De foto's heb ik vervolgens mooi gevisualiseerd met photoshop. Zo kan ik zijn dagelijks routine zien.
De photojournal heb ik gelijk naar mijn team doorgestuurd, voor het geval als zij feedback willen geven.


# Woensdag
11:00 Vanochtend heb ik met mijn team de wel bekende ochtendbespreking gedaan. Wat is de stand van zaken en wat moet er nog gedaan worden ? De interview wordt vandaag uitgevoerd.
Ik heb  studenten gevraagd uit verschillende locatie's  of hij/zij tijd hebben voor een kort interview. Aanstaande vrijdag ga ik naar Kralingse zoom om paar studenten te 
ondervragen.

13.00 Vandaag heb ik een workshop gevolgd over onderzoeksmethode. Ik vond het zeer leerzaam en nuttig voor mijn onderzoeksfase. Er kwamen verschillende methode's naar voren, waarvan
ik nooit had geweten dat ik het kon gebruiken voor mijn projecten. 

# Donderdag

# Vrijdag

# Zaterdag
# Zondag