---
title: Begin van Alpha Four [WEEK 2]
subtitle: onderzoek en concept
date: 2017-11-25
tage: ["Alpha Four"]
---


# Maandag (taakverdeling)
Vandaag hebben we gekeken naar onze opdracht van Paard in kwartaalwijzer op Natschool. Dankzij de Studioplanning op Natschool hadden we een duidelijk overzicht van de deliverables.
Deze week moest de merkanalyse en debriefing worden ingeleverd. De merkanalyse werd gemaakt door Joshoa van den Hoek en Fabian van Eijk. Mijn taak voor deze week is het maken van debriefing. 
![debrieving](../img/debrieving.jpg)
(afb.1: Een klein begin gemaakt aan debrieving)


# Dinsdag (hoorcollege)
Vandaag begon mijn 2e hoorcollege over "De mens". Een zeer interessante presentatie van Annemieke Later over "Persoonlijke beïnvloeding". Ik leerde voornamelijk over het beinvloeden van
gedrag. Als je onderzoek wilt doen en wilt proberen de mens te begrijpen in hun keuzes, dan helpt het als ik meer weet van motivatie, verlangen en behoefte en persoonlijkheid. 
Welke behoeftes en motivatie heeft een persoon bij het gebruiken van een product. De presentatie was wel lnagdurig, maar ik het zeker gebruiken voor mijn werkproces.

De aantekingen heb ik in de avonduren aangepast en overzichtelijker gemaakt voor mijn tentamen die eraan komt.

# Woensdag (debrieving)
Vandaag heb ik aan mijn debrieving gewerkt.


# Donderdag (ziek)
 
# Vrijdag (validatie)
Vandaag heb ik Engels bijles gevolgd over grammatica. Ook heb ik om 13:00 uur voor Fabian van Eijk en Joshoa van den Hoek hun merk-analyse gevolgd.
Helaas was het onvoldoende. Alles was fout, maar we hebben gelukking een korte duidelijk uitleg gekregen van Wilco. De feedback heb ik vervolgens aan Joshoa doorgegeven, zodat
hij er verder kan werken aan de merkanalyse.
![merkanalyse](../img/merkanalyse2411.jpg)
(afb.1: De 1e validatie beoordeling van merkanalyse)

# Zaterdag
# Zondag
