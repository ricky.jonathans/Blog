---
title: Merkanalyse [WEEK 3]
subtitle: onderzoek en concept
date: 2017-12-02
tage: ["paard"]
---


# Maandag
Vandaag ben ik begonnen aan de user Journey (huidige), maar wat houdt een User Journey in. Antwoord:"Ik heb geen idee".
Doormiddel van het opzoeken op het internet en de voorbeeld goed analyseren op de presentatie van Paard had ik een duidelijk beeld van een User Journey.
Ook vroeg ik aan meerdere docenten voor uitleg.

Nu ik weet wat het is en hoe ik het moet maken, ga ik aan de slag!

Verder heb ik met mijn projectgroep besproken over de taken, onderzoek en ideeen voor het concept.

# Dinsdag
Vandaag begon mijn 3e hoorcollege over “De mens”. Een zeer interessante presentatie van Isabella  over “Emotionele beïnvloeding”.
Ik leerde voornamelijk over het emotie en motivatie. Begrippen zoals positive en negatieve affect. En ook  3 niveaus van informatieverwerking.
Helaas had ik niet veel tijd om alles te noteren en aantekingen te maken.

Keuzevak: Eigen TV commercial gevold. We kregen informatie over het inhoud van een commercial en hoe je het beste kan voorbereiden. Een plan van aanpak dus.

# Woensdag
Vandaag heb ik verder gewerkt aan mijn User Journey.
![userjourney](../img/userjourney1.jpg)
(afb.1: User Journey is bijna af)


# Donderdag
Werkcollege

# Vrijdag
Vandaag heb ik weer engels bijles gevolgd over grammatica. Ook heb ik om 13:00 uur voor Fabian van Eijk en Joshoa van den Hoek hun merk-analyse gevalideerd.
En helaas was het WEER onvoldoende. We hadden klassikaal een duidelijk uitleg gekregen met voorbeelden. De feedback heb ik vervolgens aan Joshoa doorgegeven. 
Omdat ik elke keer op een vrijdag voor hun moet valideren leek het mij verstandig om ook aan de merkanalyse mee te werken.# Zaterdag
![merkanalyse](../img/merkanalyse121.jpg)
(afb.1: (afb.1: De 2e validatie beoordeling van merkanalyse)

# Zondag