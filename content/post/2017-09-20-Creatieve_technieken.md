---
title: Creative technieken voortaan toepassen bij het brainstormen
subtitle: Conclusie trekken van de mindmappen
date: 2017-09-20
tage: ["creative technieken", "brainstormen"]
---
Vandaag hebben we alle mindmap bij elkaar gezet tijdens onze brainstormsessie.
Het was een lastig en we liepen hier en daar vast. Maar we kwamen erachter dat 
we bij onze vorige iteratie te weinig op de doepgroep richten. Tijdens de interview
kregen we de informatie te weten dat de meeste 1e jaar studenten te weinig over architectuur,
constructies, bouwen enzovoort beschikken. We hebben hier en daar wat concepten bedacht, maar daar
gingen we niet verder op in.

Einde van de middag had ik de workshop creative technieken gevolgd.
Ik leerde technieken om je creativiteit op gang te houden of te brengen.
Ik heb verschillende divergerende technieken geleerd die ik kan toepassen voor mijn iteratie.
Het is vooral handig tijdens het brainstormen. Want zoals vandaag liepen we begin van de ochtend vast tijdens het brainstormen.
