---
title: Paperprotype (bordspel)
subtitle: Wat heb ik nodig voor onze prototype ?
date: 2017-09-11
tage: ["blog", "team"]
---


Vandaag heb ik samen met Donovan aan de paper prototype gewerkt. 
We hebben samen gekeke nwelke benodigdheden er nodig zijn en waar
wij rekening moeten aan houden. Samen kwamen we eruit dat we de volgende punten nodig hebben voor morgen:


- Pionnetjes of dergelijk
- Vlaggetjes
- Gebouwen 
- A3 map van rotterdam
- spelkaarten inclusief vragen
- spelregels

Vervolgens hebben we samen met heel de team verder overlegd wat we verder nodig hebben en overlegd wat er nog gedaan moet worden.

![bordspel](../img/bordspel-prototype.jpg)
(afb.1: Begin van de prototype bordspel)