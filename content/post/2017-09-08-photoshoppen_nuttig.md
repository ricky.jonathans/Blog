---
title: Wordt photoshop toch uitdagender ?
subtitle: Leren van een nieuwe tool
date: 2017-09-28
tage: ["workshop", "wk4"]
---

En yes, het is weer half 9 in de ochtend. Tijd voor nutteloze photoshop of toch niet ? Vandaag heb ik een nieuwe tools (zie afb.1) geleerd, waarmee ik krasjes of kreukels kan
verwijderen. Ook de home challene is leerzaam en uitdagender. Jezelf ouder maken met photoshop. Dat is een opdracht dat ik nog nooit heb gedaan. Photoshop begint nu
wat leerzamer te worden, maar ook leuker.

![tool](../img/tool-ps.jpg)
(afb.1: Een nieuwe tool geleerd, genaamd retoucheerpensel - class opdracht)