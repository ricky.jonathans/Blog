---
title: KILL YOUR DARLING
subtitle: BRIEFING 2
date: 2017-09-18
tage: ["blog", "concepten", "kill"]
---

Vandaag hebben wij voor 2 klassen ons concept gepresenteerd.
Helaas was Kim afwezig vanwege bepaalde redenen.
Ons concept vonden ze een goed idee, helaas kregen we weinig feedback van de klas.
De feedback die we wel kregen hadden we genoteerd. Dat gaan we gebruiken voor onze volgende iteratie.


Na onze presentatie begonnen wij met onze 2e iteratie. Het is moeilijk om een goed idee overboord te gooien, maar we besloten het toch
te doen. De doelgroep bleef, maar we zochten wel naar een nieuwe thema.
Doormiddel van branstormen en verschillende spelanalyse gaan we onderzoeken welke
thema het beste past bij de doelgroep.
