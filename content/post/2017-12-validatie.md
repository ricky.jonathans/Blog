---
title: VALIDATIE NEEM IK NIET MEER SERIEUS [WEEK 5]
subtitle: uitwerken
date: 2017-12-16
tage: ["validatie"]
---


# Maandag
Vandaag heeft Josha van den Hoek en ik aan de recap gewerkt tot s'avonds laat. We keken naar vormgeving van de recap, maar ook de inhoud. 
We vergaten helaas de antwoorden van de test(concept) te noteren via een rapportage. Mijn taak is de achterkant van de recap te ontwerpen. We hebben goed gecommuniceerd met elkaar, zodat
de voor- en achterkant goed aansluiten op elkaar.

![recap](../img/recap1.jpg)
(afb.1: achterkant van de recap)


# Dinsdag
Vandaag hadden we geen hoorcollege gehad.

# Woensdag
Vandaag is de recap gevalideert door Elske. De recap is gelukkig goed, maar er kunnen zeker aantal punten worden aangepast. We kregen als feedback dat de samenhang 
tussen de onderdelen niet aanwezig is. We hebben minder onderzoeken gedaan. Ook hebben we minder aandacht aan "trend" besteedt. 

Na de recap validatie heb ik mijn user Journey opnieuw gevalideert bij Carlo. Hij vond het goed en overzichtelijk gevisualiseerd. Er waren zeker verbeteringen aanwezig dan bij de vorige
User Journey. Alleen had ik dit keer alles fout, maar was het wel voldoende. De dag van vandaag neem ik sommige validaties niet meer serieus.

-  1e validatie User Journey van Ulla
-  ONVOLDOENDE - Zowel de voor, midden als nabeleving komen terug
-  VOLDOENDE - Maakt inzichtelijk wat gebruikers doen
-  ONVOLDOENDE - Betrouwbaar, Inzichten uit verschillende onderzoeksmethoden
-  VOLDOENDE - Maakt pijlpunten en kansen inzichtelijk
-  ONVOLDOENDE - Alle stappen die de gebruiker doorloopt, zijn er in opgenomen

Ik kan begrijpen dat mijn werk onvoldoende is.Ik moet alleen  mijn onderzoeksbronnen meenemen en aantonen en de laatste fase (nabeleving) toevoegen. Dan zou ik een voldoende krijgen.

-  2e validatie User Journey van Carlo
-  ONVOLDOENDE - Zowel de voor, midden als nabeleving komen terug
-  ONVOLDOENDE - Maakt inzichtelijk wat gebruikers doen
-  ONVOLDOENDE - Betrouwbaar, Inzichten uit verschillende onderzoeksmethoden
-  ONVOLDOENDE - Maakt pijlpunten en kansen inzichtelijk
-  ONVOLDOENDE - Alle stappen die de gebruiker doorloopt, zijn er in opgenomen

Alles fout, maar toch is de werk zwak genoteerd in plaats van onvoldoende. 

Ik heb vervolgens met mijn medestudent vergeleken en hun werk is zwak genoteerd.
Ik heb urenlang aan mijn user Journey gewerkt en ik krijg dan verschillende beoordeling en feedback.
Onlangs dat mijn werk nu wel als zwak is genoteerd, ben ik ontevreden over de validatie. Ik neem het niet meer zo serieus.


![validatie](../img/userjourney512.jpg)
(afb.1: 1e validatie beoordeling user journey huidige van Ulla Schrimbeck)
![validatie](../img/userjourney1312.jpg)
(afb.2: 2e validatie beoordeling user journey huidige van Carlo Markabun)



# Donderdag
Werkcollege

# Vrijdag
Vandaag heb ik weer engels bijles gevolgd over grammatica. Ook heb ik om 13:00 uur de merkanalyse gevalideert. Na 3,5 uur wachten is het nog steeds niet goed, maar zwak.
Volgende week weer valideren. Yesss :(.
# Zaterdag
# Zondag