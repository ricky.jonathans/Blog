---
title: Onderzoeken naar de doelgroep en thema
subtitle: Woordspin en interview
date: 2017-09-06
tage: ["blog", "mindmap"]
---

Ik had gistermiddag een afspraak gemaakt met een paar studenten of ze meewillen werken voor onze onderzoek naar de thema.
Hierbij heb ik gisteravond ook de vragen uitgetypt en meegenomen voor de interview. Om 12 uur s'middag zijn wij in de pauze naar de hogeschool rotterdam in Acedemieplein geweest.
Mijn taak was het filmen en het verwerken voor onze van de beelden.
Dankzij de beelden kon ik terugzien  wat en op welk manier ze antwoord gaven. 

Naast de visuele interview heb ik ook schriftelijk verslagje gemaakt. Doormiddel van openvragen te stellen aan verschillende 1e jaar studenten kwam ik erachter wat hun interesse zijn.

Ook heb ik thuis met plakpapier een overzichtelijk mindmap (zie afb.1) gemaak.
Ieder van onze groep heeft een individuele woordspin gemaakt. Doormiddel van de mindmappen bepalen wij ook zo de doelgroep en een thema.

![logo](../img/mindmap1.jpg)
(afb.1: Mijn overzichtelijkte mindmap over de doelgroep, thema, spel, uitwerking en rotterdam)
