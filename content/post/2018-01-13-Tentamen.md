---
title: TENTAMEN[WEEK 9]
subtitle: uitwerken
date: 2018-01-13
tage: ["tentamen"]
---


# Maandag
Yup, vandaag zaten wij aan tafel en besproken wij over de gang van zaken. Wat moest er nog gedaan worden en wie gaat het uitvoeren. Omdat er 2 high fy prototype moest worden gemaakt,
hadden wij de taken verdeeld. Joshua en ik ontwerpen de clickbare prototype en Fabian en Serhat de armbandje.

# Dinsdag
Vandaag begon mijn laatste hoorcollege over “De mens”. Een presentatie van over “Visuele beïnvloeding”.`Ik leerde oude leerstoffen van het vorige kwartaal over beeld communicatie.
Denk hierbij aan de retorica (Ethos, Phatos en Logos). Verder heb ik iets geleerd over compositie, (media)manipulatie en vectoren. Naar mijn mening vond ik het niet boeiend, totdat 
ik vanavond zelfstudie ging doen over videography.

Bijna elke avond, voordat ik naar bed ga, volg ik turtorials over videography (filming and editing). Ik vond een interessante video over compositie. Compositie maken foto's en video's voor de gebruiker
makkelijker. De kijker focus op een object/persoon doormiddel van de samenstelling van de omgeving omzicht heen. Na de video besefde ik dat de hoorcollege die ik vanochtend had toch 
zeer leerzaam is.
# Woensdag
Vandaag heb ik Fabian geholpen met de toekomstige user journey. Hij heeft weinig ervaring op het gebied van Photoshop, maar ik vond het een perfecte kans om mijn kennis te delen
met hem. Zo heb ik hem uitgelegd over de user journey en photoshop. Het was fijn om mijn kennis te delen met mijn teamgenoot. Verder heb ik een klein begin gemaakt aan de recap.

# Donderdag
Vanochtend heb ik mijn tentamen gehad. Naar mijn mening ging het best slecht. Ik denk dat ik ga herkansen.

# Vrijdag

# Zaterdag
# Zondag