---
title: Individueel onderzoek (informatie)
subtitle: Ontwerpproces, werkoverleg, onnderzoek en nog veel mee
date: 2017-09-07
tage: ["onderzoek", "team"]
---


## 3e werkoverleg  
Op donderdagochtend hebben we s'ochtend een 3e dag gepland. We gingen de bepaalde punten overleggen, zoals werkverdelen en deadlines.
Op een dondermiddag besloot ik een vragenlijst te maken en uit te werken.
De vragen moesten zodanig zijn dat ik gebruikelijk informatie eruit kon halen.
Doormiddel van openvragen en hoezo, waarom, waardoor enzovoort soortige vragen te
stellen, krijg ik meer verschillende antwoorden van de gebruiker te horen. Ik
maakte een klein verslagje van elk gebruiker. Doormiddel van deze informatie
maakte ik weer een woordspin. Zodat ik alle informatie in een overzicht te zien 
kreeg. Met behulp daarvan ging ik aan mijn collage werken.

## Ontwerproces 
```
Tijdens mijn werkcollege heb ik een ontwerpprocess gemaakt dat ik gebruik voor het maken van een productiefilmpje dat ik doe in mijn vrijetijd.
Mijn ontwerpprocess (zie afb.1) werd uitgekozen door mijn projectgroepje. Samen gingen wij kijken wat er beter kon.
![ontwerp](../img/ontwerpprocess.jpg)
(afb.1: ontwerpproces: Maken van een productiefilmpje Apollo)

Wat ik heb geleerd is dat het belangrijk is om zoveel mogelijk schetsen en tekeningen (visueel) te maken in plaats van teveel teksten.
Een ontwerpproces is belangrijk dat je de juiste stappen maakt om je product te kunnen behalen. Zo heb je een overzicht wat goed gaat en wat beter kan.


