---
title: Team Creative
subtitle: afspraken maken
date: 2017-09-04
tage: ["blog", "team"]
---

Maandag was mijn 1e officiële dag dat ik met mijn team samen ging werken aan een project.
Taken en regels waren eerst afgesproken. Ik heb een groep app gemaakt inclusief een logo (zie afb.1), zodat
wij beter kunnen communiceren .

Toen wij onze eerste literatie te horen kregen, gingen wij een brainstormsessie houden. 
Onze 1e idee was een speurtocht. We kregen wat meningen te horen, waardoor we toch 
besloten om opnieuw te bedenken.

In de pauze kwam Indy van Overdijk met een geniaal idee. Met dat idee gingen wij 
kijken welke verbeterpunten we konden toevoegen.

Onze 1e fout is dat wij niet onderzoek deden naar de thema en doelgroep. 
Mijn taak was proberen contact op te leggen met een paar studenten (gebruiker), want we gaan een onderzoeken houden naar de thema.

![creative_logo](../img/creativelogo.jpg)
(afb.1: Logo is ontworpen met behulp van photoshop)




