---
title: Photoshoppen als keuzenvak...Was dat wel een verstandige keus ? 
subtitle: Photoshop als keuzevak
date: 2017-09-14
tage: ["keuzevak", "photoshop"]
---

Yes. half 9 in de ochtend had ik mijn 1e photoshop lesje volgen. Daar heb ik zeker zin. Of toch niet ?

Mijn vaardigheden op dit moment is redelijk goed, maar ik wilde mijn kennis in het gebied van photoshop verbeteren.
De reden is, zodat ik tijdens projecten, maar ook in mijn vrijetijd betere visuele eindproducten kan maken.
Hoe beviel mijn 1e photoshop les ? Naja, het was totaal niet leerzaam. Binnen 10 minuten was ik al klaar met mijn class en home challenge .

Ik hoop dat de komende lessen meer tools kan leren.

Later in de middag volgde ik mijn 2e werkkcollege over verbeelden.
Ik had 5 afbeelding meegenomen en hieruit moest ik het waarnemen (gestalkt), begrijpen (semiotiek) en overtuigen (retorica) eruit halen.
Het was best lastig, maar dankzij de samenvatting (zie afb.1)  begreep ik de opdracht beter en ging het gelukkig goed.
Hierdoor kwam ik er ook achter dat ik dit hoofdtuk verbeelden thuis meer aandacht moet geven voor mijn tentamen.
![samenvatting](../img/werkcollegevb.jpg)
(afb.1: De samenvatting van verbeelden)

