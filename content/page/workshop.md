---
title: Workshop
subtitle: Welk workshops heb ik gevolgd ?
---
Hieronder staan de workshops die ik heb gevolgd. Helaas heb ik niet van alles bewijsmateriaal dat ik heb deelgenomen aan de workshops. 

#   bloggen (kwartaal 1)
Dankzij de workshop "bloggen" weet ik nu een makkelijk manier om te bloggen en zo kan ik mijn werkzaamheden, lessen, feedback enzovoort bij houden.

#   Moodboard (kwartaal 1)
Dankzij de workshop "moodboard" weet ik meer dan alleen plaatjes toevoegen. Ik weet dat een moodboard
een visualisatie van een concept , idee, gedachte of gevoel geeft. Het is een hulpmiddel waarmee ik keuzes kunt maken tijdens mijn project. Zeker handig voor mijn onderzoek.

#   Creatieve technieken (kwartaal 1)
Dankzij de workshop "creatieve technieken" leerde technieken om je creativiteit op gang te houden of te brengen. Ik heb verschillende divergerende technieken geleerd
die ik kan toepassen voor mijn iteratie. Het is vooral handig tijdens het brainstormen.
[Klik hier](https://ricky.jonathans.gitlab.io/Blog/post/2017-09-20-creatieve_technieken/) voor meer informatie.


#   Toevoegen van thema (blog) (kwartaal 1)
Naja, dankzij de "workshop" thema deed mijn vorige blog niet meer. Dus ik heb om eerlijk gezegd niet veel geleerd. Ik ga in mijn volgende kwartaal extra lesje volgen of
aan mijn medestudenten vragen, want op dit moment past ik CCS aan van mijn blog. Maar het is veel leuker en makkelijker om een thema toe te kunnen voegen.
[Klik hier](https://ricky.jonathans.gitlab.io/Blog/post/2017-10-03-vergeten_bloggen/) voor meer informatie.



# Merkanalyse (kwartaal 2)
 Ik heb een korte workshop gevolgd tijdens het valideren, omdat het bij velen fout gaat tijdens het maken van een merkanalyse. Ik heb veel geleerd over de functie van zo analyse (afb.1). 

# HTML/CSS deel 1 Basis van html (kwartaal 2) 
Dankzij de workshop “HTML/CSS” weet ik weer de basis van HTML5. Omdat ik weinig programmeer in mijn vrije tijd was dit een mooie kans om mijn kennis weer op te frissen. 

# HTML/CSS deel 2:  Verdiepen in CSS (kwartaal 2) 
Deel 2 van de workshop “HTML/CSS” heb ik weer gevolgd. Ik heb nieuwe weetjes geleerd over normalize (css). Normalize is een klein CSS-bestand dat zorgt voor een betere consistentie tussen de browsers in de standaardstijl van HTML-elementen. Dankzij de workshops HTML/CSS kan ik weer verder beginnen aan mijn nieuwe portfolio website die waarschijnlijk pas af is in het 3e kwartaal.

# Onderzoeksmethode (kwartaal 3)
Onderzoeksmethode was een zeer leerzame workshop voor het opschroeven van mijn kennis. Ik heb nieuwe onderzoeksmethode leren kennen die mijn onderzoeksproces kan verbeteren.  Dankzij de juiste methode te kunnen gebruiker, kan ik de juiste informatie achterhalen bij een testpersoon.

# Samenwerken (kwartaal 3)
Als team captain was het belangrijk om de samenwerking binnen mijn team te verbeteren doormiddel het te analyseren. Ik heb mijn team tijdens de workshops geanalyseerd op de manier van Model van Kaats en Opheij. Wat zijn de positieve en zwakke punten binnen de thema: “Ambitie/doel, proces, relatie, belangen en organisatie”.

# Data-visualisatie (kwartaal 3)
Om mijn onderzoek en resultaten overzichtelijker te maken, ging ik naar data-visualisatie. Helaas was de workshop op de latere moment gegeven, waarop ik mijn onderzoek al had gevisualiseerd. Onlangs dat heb ik veel inspiratie gekregen tijdens de workshop. Tijdens de workshop leerde ik over kleur, grootte, punten, typografie, beeld en beweging. Dit ga ik zeker meenemen naar kwartaal 4.

