---
title: Ik-profiel
subtitle: Wat wil ik bereiken in dit kwartaal
---



### Inleiding
Dit is mijn 1e ik-profiel. Wat zijn mijn kwaliteiten en eigenschappen ? Hier staan ook alles over mijn interesse en ervaringen. En belangrijkste, wat zijn mijn doelen.



### Gegevens
-   Voor- en achternaam: 	Ricky Jonathans
-   Adres: 		        	Snoekenveen 836, Spijkenisse
-   Email: 		        	0940100@hr.nl
-   Geboortedatum:      	24-03-1996
-   Nationaliteit: 	    	Nederlandse



### Kwaliteiten:
-	Mijn positieve punten zijn doorzettingsvermogen en leergierig. Voor elk project (ook buiten mijn opleiding) wil ik tot het uiterste gaan. 
-	Maximale inzet voor het behalen van het hoogste haalbare resultaat. Wanneer het niet voldoende is voor mij zoek ik altijd naar de eventuele
-	oorzaak zodat ik de volgende keer kan verbeteren
-	Ik hou van zeer netjes werken. Duidelijk planning maken en overzichtelijk werken zorgen voor een goed product.  
-	Samenwerken vind ik belangrijk, zodat je samen een gemeenschappelijk doel gaat bereiken.
-	Dankzij mijn vooropleiding en zelfstudie heb ik een redelijke voorsprong in het gebied van HTML, CSS, 
-	tekenvaardigheden (praktijk en theoretisch) tot Adobe programma’s (Photoshop, Premiere en Lightroom). 



### Opleidingen:
De Ring van Putten: TL-VMBO (profiel Economie) inclusie tekenen als examenvak
Albeda 		      : ICT beheerder niveau 4  (van ander opleiding gegaan)
Zadkine		      : Applicatie- Mediadeveloper (Gamedeveloper ) niveau 4



### Werkervaring: 
Ik werk momenteel al 6 jaar bij de jumbo van vakkenvuller tot vulploegleider.  Verder heb ik geen werkervaring.



### Interesse:
In mijn vrije tijd maak ik filmpjes van aftermovies tot promotie filmpjes voor een bedrijf Apollo protocol. 
Hierdoor werk ik veel met Adobe Premiere, photoshop en Lightroom.
Doormiddel van tutorial opzoeken op YouTube leer ik naast mijn studie ook andere kennissen.

Tekenen deed ik vroeger heel vaak. Van gedetailleerd gezichten tekenen tot architectuur (gebouwen). 
Helaas heb ik afgelopen niet meer vaak gedaan. Ik ben van plan mijn tekenvaardigheden weer op te pakken .
Verder doe in mijn vrije tijd fitnessen.

### Wat wil je bereiken met de opleiding?
Op dit moment wil ik verdiepen over bepaalde kennis en vaardigheden. Ik wil mijn vaardigheden verbeteren 
zoals tekenen , maar ook het verbeteren van onderzoeken tot werken van een project met de juiste proces.

### Wat zijn je doelen voor het eerste kwartaal/ eerste leerjaar?
Mijn doel voor dit kwartaal is het behalen van al mijn studiepunten. Naast dat wil ik graag nog beter de 
klas leren kennen.  Samenwerken met andere studenten en van elkaar leren. In het eerste jaar wil ik zeker zijn 
dat ik verder wil met de opleiding wil. Ook wil ik mijn Propedeuse halen.



#### Leerdoelen van kwartaal 1:

-  Zoveel mogelijk studiepunten halen in het 1e kwartaal
-  Teken advies C behalen
-  Nederlands en engels voldoende halen
-  Project voldoende afronden
-  Tentames en toetsen behalen
-  Manier van samenwerken en zelfstandig werken verbeteren



#### Welke stappen ga ik ondernemen om de bovenstaande doelen te bereiken
-  Verdiepen in het ontwerpproces
-  Verdiepen in het onderzoeken van een doelgroep, thema etc.
-  Tekenen oppakken doormiddel vaker te schetsen in mijn dummy
-  Photoshop kennis verbeteren als dat mogelijk is
-  Feedback vragen om mijn verbeterpunten te achterhalen en op basis daarvan extra lessen te volgen
-  Ontbrekende kennis opzoeken doormiddel van workshops, keuzevakken, hoorcollege, aantekeningen, vragen stellen en zelfstudie
-  De juiste keuzevakken kiezen, zodat ik dat kennis en ervaring kan gebruiken voor mijn projecten
-  Goed communiceren en plannen met mijn projectgroepje


#### Leerdoelen van kwartaal 2:
- 	Verdiepen in research (onderzoeken)
- 	Verdiepen in het maken van commercial video’s
- 	Tekenen advies D volgen als het mogelijk is
- 	Engels voldoende halen
- 	Feedback die ik heb gekregen van kwartaal 1 meenemen naar kwartaal 2 en verwerken
- 	Manier van samenwerken  en zelfstandig werken verbeteren 







####Welke stappen ga ik ondernemen om de bovenstaande doelen te bereiken
- 	Workshop volgen dat gericht is op “onderzoeken”.
- 	Zelfstudie, blogs lezen over usability-onderzoek, user experience research en verschillende tools die ik kan gebruiken voor dit project.
- 	Hoorcollege volgen en de theorieën van de hoorcollege gebruiken in het project.
- 	Photoshop en Premiere  kennis vergroten doormiddel van zelfstudie (tutorial)
- 	Eigen tv-commercial volgen
- 	Kwaliteit en kennissen van mijn projectgroep leren kennen om ze te gebruiken voor het project.


# Heb ik het gehaald of niet ?   
Welke doelen heb ik bereikt na het einde van mijn 1e kwartaal. Lees hier voor meer informatie [...](...)
