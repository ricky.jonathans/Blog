---
title: Leerproces
subtitle: Feedback en verbeteringen
---

# Feedback en verbeteringen
1e kwartaal zit erop. Wat heb ik geleerd ? En welke feedback heb ik ontvangen ? Zijn er
verbetertpunten die ik kan oppakken in de 2e kwartaal.En hoe ging het verder ?
Feedback vragen aan elkaar is zeker belangrijk binnen de groep. Hoe beviel de
samenwerking tot nu toe en wat kan ik beter voor de volgende keer. 
Doormiddel van een peerfeedback formulier kon ik zien wat mijn projectgroep
over mij dachten. Dat kun je hieronder vinden.

### 1e  Peerfeedback 

Zoals je kan zien op de peerfeedback formulier (afb.1 en afb.2) zijn ze zeer 
positief over mijn functioneren in de samenewerking.
Maar wat ik belangrijk vind, zijn de verbeterpunten.
Mijn verbeterpunten zijn:

-   Je weet jezelf en anderen te motiveren in lastige situatie (Indy)

-   Je weet discussies consensus te bereiken (Donovan en Kim)

Conclusie: Kim en Donovan vinden dat ik soms te hoog op loop tijdens het discussie en ik kan
dat zeker voorstellen. Ik moet beter brainstormen met mijn projectgroep.
Ik moet minder ideeen afkraken en goed luisteren naar elkaar. Ook mijn meningen en ideeen vaker delen
en overtuigen.

Indy vindt dat ik laat op de middag uren  minder motivatie heb om verder te werken en dat ik stop.
Dat kan ik zeker begrijpen. Gelukkig werk ik in mijn vrijetijd verder aan mijn werkzaamheden, zodat
we niet achterlopen. Maar dat is geen excuses en ik moet zeker eraan werken om ook laat in de middaguren
actief te zijn met het werken aan mijn opdrachten

Zoals je kan zien heb ik verder best weinig feedback gekregen. Dat vind ik wel jammer.
ik hoop op de volgende peerfeedback meer verbeterpunten 
te krijgen.

![peerfeedback1a](../img/peerfeedback1a.jpg)
(afb.1 peerfeedback formulier voorkant van week 5 )

![peerfeedback1b](../img/peerfeedback1b.jpg)
(afb.2 peerfeedback formulier achterkant van week 5 )


### 2e  Peerfeedback 
De eerste kwartaal is bijna afgelopen. Bij mijn volgende 2e peerfeedback (afb.1 en afb.2) heb ik gelukkig meerdere 
verbeterpunten gekregen.

De volgende verbeterpunten van 2e peerfeedback zijn:

-   Je respecteert deadlines en komt deadlines en afspraken na.(Kim)

-   Je weet jezelf en anederen te motiveren in lastige situatie. (Indy, Kim en Lars)

-   Je weet in discussies consensus te bereiken. (Indy, Kim en Lars)

-   Je overtuigt de opdrachtgever, teamgenoten en docenten van eigen ideeen. (Indy)


En ook hierin heb ik mijn manier van discusseren niet verbetert. De reden was voornamelijk dat ik mijn ideeen niet te vaak overtuigde en deelde.
Wel heb ik het geprobeerd om mijn concept visueel te overtuigen[klik hier](https://ricky.jonathans.gitlab.io/Blog/post/2017-09-22-uitwerken/).
Maar ik moet mijn concept ook tijdens het brainstormen kunnen delen en overtuigen zonder zo uitgebreid visueel beeldmateriaal.

Ook waren mijn verbeterpunten het niet optijd halen van een deadline. Zoals je kan zien op de peerfeedback (zie afb.4) heb ik zeker veel werk gestoken in het editen, filmen,
powerpoint, concepten (photoshoppen) etc, waardoor ik vaak de deadlines niet haalde. Een oplossing hiervoor is, bijvoorbeeld beter plannen of een goed werkverdeling maken.

Ik ga zeker deze verbeterpunten in de komende maanden verbeteren. 

![peerfeedback2a](../img/peerfeedback2a.jpg)
(afb.3 peerfeedback formulier voorkant van week 8 )

![peerfeedback2b](../img/peerfeedback2b.jpg)
(afb.4 peerfeedback formulier voorkant van week 8 )

