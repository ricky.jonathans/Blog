---
title: Zelfreflectie
subtitle: Reflecteren op kwartaal 1
---

1e kwartaal is eindelijk voorbij. Ik heb de afgelopen maanden veel geleerd, waaronder dat je fouten moet kunnen maken om jezelf te ontwikkelen. Feedback 
accepteren en er wat meedoen. Niet naar complimentjes zoeken, maar naar verbeteringen. Dat was voor mij het moeilijkste, want soms komt een feedback over naar 
een kritiek. En dat moet ik zeker niet zo zien. Verder was het samenwerken in een lange termijn even wennen. Takenverdeling en planning was een groot belang binnen
onze literatie's. Maar we hebben ons project goed afgesloten met ze alle.

De kennis en vaardigheden die ik heb geleerd afgelopen tijd was ook zeer nuttig. Van hoorcollege naar een nuttige photoshop lesje vroeg in de ochtenduren.
Workshop geleerd over moodboards en het tekenen van een hand. Ik weet wat voor kennis en vaardigheden ik nog mis dankzij de nul-meting, maar ook de feedbacks 
die ik heb gevraagd en gekregen. Ik ben benieuwd wat er in de komende kwartalen ga beleven. 

Welke stappen ga ik nu ondernemen:
-   Taakverdelen en beter plannen
-   Mijn ideeen beter met de groep delen en overtuigen
-   Meer workshop over onderzoeken en bloggen volgen 
-   Illustrator en Indesign volgen
-   Tekenen C advance toch gaan volgen
-   Meer verdiepen over ontwerpen en welke technieken kan ik gebruiken voor mijn komende literatie's
-   Mijn concepten op een professionele manier laten zien
-   Opzoek naar een wow factor dan in plaats van tevredenheid. Hoe ga ik dat bereiken ?
-   Nog meer kennis opzoeken naar inspiratiebronnen


Ik hoop dat ik deze stappen ga maken in de komende kwartaal. Ik ben nu tevreden over mijn keuze van mijn opleiding. Ik hoop dat ik deze motivatie blijf houden, zodat ik
in de toekomst mezelf beter kan ontwikkelen.

---
subtitle: Reflecteren op kwartaal 1
---
2e kwartaal is voorbij. Ik heb de afgelopen maanden veel geleerd van mijn validatie's (feedback) en de werkzaamheden die ik heb verricht.
Samenwerken in een nieuw team was even wennen. We waren zeer slecht begonnen. De taken en deadline werden niet gehaald. Ons concept was niet uniek en goed genoeg en 
we zaten in een knelpunt.

We hebben 2 keer "kill your darling" gedaan. Maar gelukkig is 3 keer scheeprecht. Een nieuw uniek concept hadden we bedacht. We hebben het vervolgens goed en 
proffessioneel mogelijk uitgevoerd.


Naast mijn design challenge heb ik ook keuze vak "eigen commercial gevolgd". De keuzevak tv-commercial was redelijk leerzaam. Ik weet nu waar ik moet letten tijdens het maken van een commercial video. Dat kan
handig zijn voor mijn volgende project en voor mijn vrijetijd besteding. 

Welke stappen ga ik nu ondernemen:
-   Scrum vaker gebruiken voor de taken verdelen en beter plannen
-   Vaker feedback vragen over mijn gemaakte werk
-   Keuzevak "making movie volgen"
-   Portofolio website programmeren en online zetten
-   Portofolio uitbreiden
-   Blog volgens over creative technieken/onderzoeksmethode
-   Tools design "Indesign" volgen
-   Meer focussen over de compententie die ik wil behalen in het 3e kwartaal
-   Tekenskills verbeteren (D niveau)

Ik hoop dat ik deze stappen ga behalen in het 3e kwartaal. De 3e en 4e kwartaal wil ik me zoveel mogelijk mezelf ontwikkelen. Ook buiten mijn opleiding.

---
subtitle: Reflecteren op kwartaal 3
---
Kwartaal 3 is voorbij en ik heb veel kennis opgedaan. Mijn ontwerpproces is zeker verbetert ten opzichte van de kwartaal 1 en kwartaal 2. Wat zijn mijn vooruitgangen ?
Ik heb in dit kwartaal verdiep in het onderzoek. Verschillende methode en technieken heb ik geleerd en uitgevoerd. Ook de samenwerken is verbeter ten opzicht van kwartaal 1.
Donovan en Lars heb ik al bij de 1e kwartaal meegewerkt en het ging een stuk beter dan ik had gehoopt.  

De werkzaamheden en proces die wij verliepen ging goed. Natuurlijk waren er punten dat beter kon gaan en dat ga ik zeker aan werken in mijn laatste kwartaal.

- In mijn komende kwartaal wil ik meer methodes en technieken leren om mijn 
ontwerpproces te versterken
- Ik wil nog creativer worden, door meer inspiratie te zoeken van andere mede-
studenten. Doormiddel van hun gemaakte werk te bekijken en te analyseren.
- Ik wil mijn Adobe kennis verbreden zoals Indesign en illustrator, maar ook verdiepen zoals Adobe premiere en photoshop. 
- Mijn doorzetten en verdiepen in de compenties die ik wil behalen in de komende kwartaal.
- Elke studiodagen wil ik graag feedback vragen en ontvangen. Ik vind dat zeker beter kan.

De bovenste punten wil ik zeker eraan werken. Ook ga ik mijn nieuw website in elkaar ontwerpen voor mijn portofolio. Mijn dagelijkse proces en werkzaamheden zal je de volgende kwartaal zien op www.rifflino.nl
